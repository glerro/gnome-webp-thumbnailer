# WebP Thumbnailer
This is a thumbnailer for GNOME desktop that will generate thumbnails for WebP images format,
but can be called manually as well.

## Dependencies
- `glib-2.0`
- `gdk-pixbuf-2.0 ≥ 2.6.0`
- `gio-2.0`
- `libwebp`

## Building and Installing
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

In order to build you will need to have istalled gcc, git, meson and ninja.

For a regular use these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-webp-thumbnailer.git
cd gnome-webp-thumbnailer
meson setup --prefix=/usr _build .
meson compile -C _build
meson install -C _build
```

## Stand-alone Usage 
The sintax is:

```bash
gnome-webp-thumbnailer -s size input-image.webp output-thumbnail.png
```
The parameter -s size allows the maximum height and width to be specified (in pixels).

## Acknowledgments
This project is based on [gnome-thumbnailer-skeleton](https://github.com/hadess/gnome-thumbnailer-skeleton)
a great project of Bastien Nocera.

## License
Gnome Webp Thumbnailer - Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>

Gnome Webp Thumbnailer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gnome Webp Thumbnailer is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Gnome Webp Thumbnailer. If not, see <https://www.gnu.org/licenses/>.
