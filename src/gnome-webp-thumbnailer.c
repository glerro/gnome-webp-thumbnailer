/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Gnome Webp Thumbnailer.
 * https://gitlab.gnome.org/glerro/gnome-webp-thumbnailer
 *
 * gnome-webp-thumbnailer.c
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Gnome Webp Thumbnailer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gnome Webp Thumbnailer is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Gnome Webp Thumbnailer. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#include <stdlib.h>
#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <webp/decode.h>

const char *vp8StatusCodeNames[] =
{
    "VP8_STATUS_OK",
    "VP8_STATUS_OUT_OF_MEMORY",
    "VP8_STATUS_INVALID_PARAM",
    "VP8_STATUS_BITSTREAM_ERROR",
    "VP8_STATUS_UNSUPPORTED_FEATURE",
    "VP8_STATUS_SUSPENDED",
    "VP8_STATUS_USER_ABORT",
    "VP8_STATUS_NOT_ENOUGH_DATA"
};

static void
pixbuf_destroy_data (guchar  *pixels,
                     gpointer data)
{
    /* From https://developers.google.com/speed/webp/docs/api
     * the data buffer created with WebPDecodeRGBA and WebPDecodeRGB
     * must be delete with WebPFree
     */
    WebPFree ((void *) pixels);
}

GdkPixbuf *
file_to_pixbuf (const char *path,
                guint       output_size,
                GError    **error)
{
    GFile *input_file;
    GFileInputStream *input_file_stream;
    GFileInfo *input_file_info;
    guint64 input_file_size;
    guint8 *input_file_data;
    WebPBitstreamFeatures webp_features;
    VP8StatusCode webp_status;
    gboolean use_alpha = TRUE;
    guint8 *img_data = NULL;
    gint img_width, img_height;
    GdkPixbuf *pixbuf = NULL;

    /* Open the WebP image file for reading */
    input_file = g_file_new_for_path (path);
    input_file_stream = g_file_read (input_file, NULL, error);
    g_object_unref (input_file);

    if (input_file_stream == NULL)
    {
        return NULL;
    }

    /* Queries the file input stream to get attributes (file size) */
    input_file_info = g_file_input_stream_query_info (input_file_stream, "standard::", NULL, error);
    input_file_size = g_file_info_get_size (input_file_info);
    g_object_unref (input_file_info);

    /* Read the content of the stream into the buffer */
    input_file_data = g_malloc (input_file_size);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), input_file_data, input_file_size, NULL, error) < 0)
    {
        return NULL;
    }
    g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
    g_object_unref (input_file_stream);

    /* This function will retrieve features from the WebP image bitstream */
    webp_status = WebPGetFeatures (input_file_data, input_file_size, &webp_features);
    if (webp_status == VP8_STATUS_OK)
    {
        use_alpha = webp_features.has_alpha;

        if (use_alpha)
        {
            /* This functions decode a WebP image with alpha channel */
            img_data = WebPDecodeRGBA (input_file_data, input_file_size, &img_width, &img_height);
        }
        else
        {
            /* This functions decode a WebP image without alpha channel */
            img_data = WebPDecodeRGB (input_file_data, input_file_size, &img_width, &img_height);
        }

        g_free (input_file_data);

        if (!img_data)
        {
            g_warning ("Cannot decode WebP image");
            return NULL;
        }

        /* Creates a new GdkPixbuf from WebP image data */
        pixbuf = gdk_pixbuf_new_from_data ((const guchar *) img_data, GDK_COLORSPACE_RGB,
                                           use_alpha, 8, img_width, img_height,
                                           img_width * (use_alpha ? 4 : 3),
                                           pixbuf_destroy_data,
                                           NULL);

        if (!pixbuf)
        {
            WebPFree ((void *) img_data);
            return NULL;
        }

        return pixbuf;
    }
    else
    {
        g_free (input_file_data);
        g_warning ("Cannot get WebP image features - Error: %s", vp8StatusCodeNames[webp_status]);
        return NULL;
    }
}

