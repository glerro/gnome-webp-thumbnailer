/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Gnome Webp Thumbnailer.
 * https://gitlab.gnome.org/glerro/gnome-webp-thumbnailer
 *
 * gnome-thumbnailer-skeleton.h
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Gnome Webp Thumbnailer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gnome Webp Thumbnailer is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Gnome Webp Thumbnailer. If not, see <https://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 * Original Author: Bastien Nocera <hadess@hadess.net>
 * *****************************************************************************
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#include <gdk-pixbuf/gdk-pixbuf.h>

#ifdef THUMBNAILER_RETURNS_PIXBUF
#ifdef THUMBNAILER_RETURNS_DATA
#error "Only one of THUMBNAILER_RETURNS_PIXBUF or THUMBNAILER_RETURNS_DATA must be set"
#else
GdkPixbuf *
file_to_pixbuf (const char *path,
                guint       destination_size,
                GError    **error);
#endif
#elif THUMBNAILER_RETURNS_DATA
char *
file_to_data (const char *path,
              gsize      *ret_length,
              GError    **error);
#else
#error "One of THUMBNAILER_RETURNS_PIXBUF or THUMBNAILER_RETURNS_DATA must be set"
#endif

